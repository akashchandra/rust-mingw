FROM debian:stretch-slim
LABEL maintainer="akash.chandra@live.com"

# Make toolchain an argument so that it can be changed
# if needed while running `docker build .`
ARG TOOLCHAIN=nightly-2019-01-04

# Setup environment for Rust installation
ENV RUSTUP_HOME=/usr/local/rustup \
    CARGO_HOME=/usr/local/cargo \
    PATH=/usr/local/cargo/bin:$PATH

# Add 32 bit architecture for testing and build
RUN dpkg --add-architecture i386
# Install build tools and other dependencies
RUN set -eux; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        ca-certificates \
        gcc \
        libc6-dev \
        curl \
        mingw-w64 \
        wine64 \
        wine32

# Download the installer for rust
RUN curl https://sh.rustup.rs --output /tmp/rustup-init.sh && chmod +x /tmp/rustup-init.sh;

# Download
RUN /tmp/rustup-init.sh -y --no-modify-path --default-toolchain ${TOOLCHAIN}

# Add other architectures for build and testing
RUN rustup target add i686-unknown-linux-gnu
RUN rustup target add x86_64-pc-windows-gnu
RUN rustup target add i686-pc-windows-gnu

RUN rustup --version; \
    cargo --version; \
    rustc --version; \
    \
    apt-get remove -y --auto-remove \
        curl \
        ; \
    rm -rf /var/lib/apt/lists/*;

# Copy config for cargo
ADD config /usr/local/cargo
